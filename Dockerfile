# miweki9011/gitlab-runner-default image
FROM alpine:3.7

RUN apk --no-cache add bash curl
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

ENTRYPOINT ["/usr/bin/env"]
